# TP 1 Formula Resolvente

_Organización del computador II -       UNGS 2
do cuatrimestre del 2021_


_alumno: Cueva Melina Alejandra_



# Objetivos
 Realizar un programa en assembler IA32 que calcule las raíces de una función cuadrática a través de la fórmula resolvente.




> En la siguiente imagen se puede observar la Formula:

![Screenshot](resolvente.jpg)



 
-  Hacer un programa en C, que solicite al usuario estos valores a,b y c y luego invocar a la
función desde C.

-  Compilar y linkear los archivos objeto de manera separada. Obtener un ejecutable que
muestre por consola las raíces obtenidas.


-  Escribir una función en assembler IA-32 que reciba un número r y un puntero a un
vector de números de punto flotante, que calcule el producto escalar. Debe multiplicar
cada elemento del vector por r.


      Asumir:
            
> - 𝑏**2 − 4𝑎𝑐 ≥ 0, ∀ 𝑎, 𝑏, 𝑐 ϵ 𝑅
> - 𝑎 > 0, 𝑎 ϵ 𝑅

# Terminal Linux
Para la compilación del código utilizamos :
- para assembler: NASM
- para C : gcc. 



- [ ] Para clonar el repositorio utilice los siguientes comandos:
> sudo apt update

> sudo apt-get install git

> git clone https://gitlab.com/cuevamelina/tp1-formula-resolvente-orga2.git


- [ ] Para el uso de nasm:

> sudo apt-get install nasm 

- [ ] Para el uso  gcc:

> sudo apt-get install gcc 


# Codigo C


Lo primero que se realiza es importar la libreria y declarar la funcion de la Formula.
Luego paso por paramentros los valores a, b y c y se recibe los punteros *raiz1 y *raiz2, los cuales se encargan de almacenar el valor de las raíces posibles.

> La función retorna un 0 si no existieran raíces posibles dentro del conjunto de los reales, y 1 en caso de que sí existan.> 

Se declaran las variables a, b y c y se le pide al usuario que ingrese el valor de cada una. 
> Utilizamos un int tieneSolucion que nos indicará si la ecuacion tiene soluciones posibles.> 

```
float a;
float b;
float c;

float raiz1;
float raiz2;

int tieneSolucion;

printf("\nIntroduzca el valor de a: ");
scanf("%f",&a);
printf("\nIntroduzca el valor de a: ");
scanf("%f",&b);
printf("\nIntroduzca el valor de a: ");
scanf("%f",&c);
```

Al finalizar llamamos a  la función y mostramos por consola las raíces obtenidas. Se indica en caso de que el resultado sea una raíz doble.tieneSolucion = formulaResolvente(a,b,c,&raiz1,&raiz2);

    if(hayRaices == 1){
      if(raiz == raiz2){
         printf("\nLa función tiene una raíz doble en: {%f}\n",raiz1);
      }
      else{
         printf("\nEl conjunto de raíces es: {%f, %f}\n",raiz1,raiz2)
      }
    }
    else{
        printf("No existen raices dentro de los numeros reales");
    }





# Código Assembler IA32
Los parametros son al almacenados en la pila cuando la funcion es llamada desde c.

_Nota: Para poder obtener estos valores contamos con el registro EBP que apunta a la base de la pila, y el registro ESP que apunta al tope de la misma._

Comienzo declarando las variables. En este caso solo necesito almacenar el valor -4 en una variable que luego utilizo durante el desarrollo de la fórmula resolvente.

```
section .data
   neg dw -4
```

D eclaro la función como global, para que luego se vincule la funcion con la llamada en C:

 ```
section .text
 global formulaResolvente
```

Comienzo realizando el Enter 0,0.

```
formulaResolvente:
   push ebp
    mov ebp,esp
```
Formula:
Utilizo la FPU mediante su set de instrucciones ya que loss valores pueden ser puntos flotantes.
Luego almaceno el valor de 2a en la pila para no repetir el código al calcular la segunda raíz.

  ```
 fld1                 ; 1
   fld dword[ebp+8]     ; a,1
   fscale               ; 2a,1
   fdivp st1            ; 1/(2a)
   fstp dword[ebp-12]   ;Guardo 1/(2a) y vacio el stack
    
   fild word[neg] ; -4
   fld dword[ebp+8]     ; a,-4
   fmulp st1            ; (-4a)
   fld dword[ebp+16]    ; c,(-4a)
   fmulp st1            ; -4ac
   fld dword[ebp+12]    ; b,-4ac
   fld dword[ebp+12]    ; b,b,-4ac
   fmulp st1            ; b^2,-4ac
   faddp st1            ; b^2 - 4ac
```

Verifico que b^2-4ac >= 0, para saber si existen soluciones. Si esta condición no se cumple se produce un salto a la etiqueta noHayRaices.

  ```
 ftst                ; Compara el primer valor del stack con 0
   fstsw ax            ; Guarda el status de la fpu en ax
   sahf                ; Copia SF,ZF,AF,PF y CF desde AH hacia el registro de flags.
   jb noHayRaices      ; Si el resultado es negativo no se puede calcular la raiz.
```

De existir raíces, se prosigue almacenando sqrt(b^2-4ac) y formando la fórmula resolvente.

   ```
fsqrt               ; sqrt(b^2 - 4ac)
   fst dword[ebp-8]    ; Guardo el resultado de la raiz cuadrada y vacio el stack
    
   fld dword[ebp+12]   ; b, sqrt(b^2 - 4ac)
   fchs                ; -b, sqrt(b^2 - 4ac)
   faddp st1           ; -b + sqrt(b^2 - 4ac)
   fld dword[ebp-12]   ; 1/(2a), -b + sqrt(b^2 - 4ac)
   fmulp st1           ; (-b + sqrt(b^2 - 4ac)) / 2a
   mov ebx, [ebp+20]   ; Almaceno en ebx la direccion de la primer raiz
   fstp dword[ebx]     ; Guardo el valor obtenido en raiz1 y vacio el stack
```


 > nota: el proceso para obtener la segunda raíz es similar, solo hay que cargar los valores obtenidos previamente en 2a y sqrt(b^2 - 4ac) y realizar la resta en vez de la suma.

> 
```
   fld dword[ebp-8]    ; sqrt(b^2 - 4ac)
    fchs                ; -sqrt(b^2 - 4ac)
    fld dword[ebp+12]   ; b, -sqrt(b^2 - 4ac)
    fchs                ; -b, -sqrt(b^2 - 4ac)
    faddp st1           ; -b - sqrt(b^2 - 4ac)
    fld dword[ebp-12]   ; 1/(2a), -b - sqrt(b^2 - 4ac)
    fmulp st1           ; (-b - sqrt(b^2 - 4ac)) / 2a
    mov ebx, [ebp+24]   ; Almaceno en ebx la direccion de la segunda raiz
    fstp dword[ebx]     ; Guardo el valor obtenido en raiz2 y vacio el stack
```


> nota :en el caso de que no existan soluciones se almacena el valor 0 en el registro eax.
> 

noHayRaices:
    mov eax,0
    jmp end

 una vez que son calculadas las raíces, se realiza un salto a la etiqueta end donde se produce el Leave.

end:
   pop ebp
   ret
   
# Compilación :
Para compilar el  programa y linkear C con NASM se debe utilizar los siguientes comandos:

1. Utilizamos el comando:

> nasm -f elf32 formulaResolvente.asm -o formulaResolvente.o 

2. Linkeamos nasm con C con:
> gcc -m32 formulaResolvente.c formulaResolvente.o 



# Ejecución
Para ejecutar el programa utilizamos la siguiente instrucción en la terminal:


> ./formulaResolvente
> 

Y para depurar el código utilizamos DDD para depurar el código, observando su comportamiento y el uso de los registros:

> ddd formulaResolvente> 

# Ejemplo
![Screenshot](ejemplo1.jpg)

--------------------------------------------

# Producto Escalar
Esta función en assembler IA-32 recibe un número r y un puntero a un vector de números de punto flotante,
y tiene que calcular el producto escalar. Debe multiplicar cada elemento del vector por r

# Compilacion y ejecucuión
> nasm -f elf32 productoEscalar.asm -o productoEscalar.o;
> 
> gcc -m32 escalarPrud.c escalarProd.o -o execProduct;
> 
> ./execProduct;

# Ejemplo 
![Screenshot](ejemploProd.jpg)

# Conclusion
Se utilizo las herramientas y conceptos trabajados durante clase. Y gracias tambien a los videos se pudo desarrollar las dos tareas solicitadas. 


